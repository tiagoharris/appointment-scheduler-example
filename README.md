# appointment-scheduler-example

A demo project to implement an appointment scheduler using Spring Boot and Quartz.

## Stack:
* [Spring Boot](https://spring.io/projects/spring-boot)
* [Quartz](http://www.quartz-scheduler.org/)
* [Maven](https://maven.apache.org/)


