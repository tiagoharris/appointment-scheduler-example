package com.tiago.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

/**
 * Utility class that deals with dates.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Component
public class DateUtil {

  private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy 'at' H:mm a");
  
  /**
   * Formats a {@link LocalDateTime} to String.
   * 
   * @param localDateTime
   * @return String
   */
  public static String toString(LocalDateTime localDateTime) {
    return localDateTime.format(FORMATTER);
  }
}
