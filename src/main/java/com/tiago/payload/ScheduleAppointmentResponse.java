package com.tiago.payload;

import java.time.LocalDateTime;

/**
 * Encapsulates appointment response data.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public class ScheduleAppointmentResponse {
  
  private String appointmentId;
  
  private LocalDateTime scheduledDateTime;

  public String getAppointmentId() {
    return appointmentId;
  }

  public void setAppointmentId(String appointmentId) {
    this.appointmentId = appointmentId;
  }

  public LocalDateTime getScheduledDateTime() {
    return scheduledDateTime;
  }

  public void setScheduledDateTime(LocalDateTime scheduledDateTime) {
    this.scheduledDateTime = scheduledDateTime;
  }

}
