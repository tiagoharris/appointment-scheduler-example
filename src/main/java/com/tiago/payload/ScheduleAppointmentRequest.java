package com.tiago.payload;

import java.time.LocalDateTime;
import java.time.ZoneId;

import javax.validation.constraints.Email;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Encapsulates appointment request data.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public class ScheduleAppointmentRequest {

  @NotBlank(message = "'name' is missing")
  private String name;
  
  @NotBlank(message = "'email' is missing")
  @Email
  private String email;
  
  @NotNull(message = "'appointmentDateTime' is missing")
  @FutureOrPresent(message = "'appointmentDateTime' must be after current date and time")
  private LocalDateTime appointmentDateTime;
  
  @NotNull(message = "'timeZone' is missing")
  private ZoneId timeZone;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public LocalDateTime getAppointmentDateTime() {
    return appointmentDateTime;
  }

  public void setAppointmentDateTime(LocalDateTime appointmentDateTime) {
    this.appointmentDateTime = appointmentDateTime;
  }

  public ZoneId getTimeZone() {
    return timeZone;
  }

  public void setTimeZone(ZoneId timeZone) {
    this.timeZone = timeZone;
  }
}
