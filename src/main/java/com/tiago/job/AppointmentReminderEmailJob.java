package com.tiago.job;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.tiago.mailer.Mailer;

/**
 * Job class to send appointment reminder email.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Component
public class AppointmentReminderEmailJob extends QuartzJobBean {

  @Autowired
  Mailer mailer;
  
  private static final Logger LOGGER = LoggerFactory.getLogger(AppointmentReminderEmailJob.class);
  
  private static final String SUBJECT_TEMPLATE = "%s, you have an appointment: %s";
  
  private static final String TEXT_TEMPLATE = "Hi %s, <br><br> Just to remember that you have an appointment: <b>%s</b>. <br><br> See you soon!";
  
  @Override
  protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
    LOGGER.info("Executing Job with key {}", jobExecutionContext.getJobDetail().getKey());

    JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
   
    String name = jobDataMap.getString("name");
    String recipientEmail = jobDataMap.getString("email");
    String scheduledDate = jobDataMap.getString("scheduledDate");
    String subject = String.format(SUBJECT_TEMPLATE, name, scheduledDate);
    String body = String.format(TEXT_TEMPLATE, name, scheduledDate);

    mailer.sendMail(name, recipientEmail, subject, body);
    
    LOGGER.info("Done execution of Job with key {}", jobExecutionContext.getJobDetail().getKey());
  }
}
