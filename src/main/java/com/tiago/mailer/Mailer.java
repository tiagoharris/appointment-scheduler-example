package com.tiago.mailer;

import java.nio.charset.StandardCharsets;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

/**
 * Utility class to send email.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Component
public class Mailer {

  @Autowired
  private JavaMailSender mailSender;

  @Autowired
  private MailProperties mailProperties;
  
  private static final Logger LOGGER = LoggerFactory.getLogger(Mailer.class);
  
  public void sendMail(String name, String toEmail, String subject, String body) {
    try {
      LOGGER.info("Sending Email to {}", toEmail);
      MimeMessage message = mailSender.createMimeMessage();

      MimeMessageHelper messageHelper = new MimeMessageHelper(message, StandardCharsets.UTF_8.toString());
      messageHelper.setSubject(subject);
      messageHelper.setText(body, true);
      messageHelper.setFrom(mailProperties.getUsername());
      messageHelper.setTo(toEmail);

      mailSender.send(message);
      
      LOGGER.info("Email sent to {}", toEmail);
    } catch (Exception ex) {
      LOGGER.error("Failed to send email to {}: {}", toEmail, ex.getMessage());
    }
  }
}
