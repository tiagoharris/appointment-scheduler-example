package com.tiago.service;

import org.quartz.SchedulerException;

import com.tiago.payload.ScheduleAppointmentRequest;
import com.tiago.payload.ScheduleAppointmentResponse;

/**
 * Service to schedule appointments.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public interface ScheduleAppointmentService {

  /**
   * Schedules an appointment.
   * 
   * @param request {@link ScheduleAppointmentRequest}
   * @return {@link ScheduleAppointmentResponse}
   * @throws SchedulerException
   */
  ScheduleAppointmentResponse scheduleAppointment(ScheduleAppointmentRequest request) throws SchedulerException;
}
