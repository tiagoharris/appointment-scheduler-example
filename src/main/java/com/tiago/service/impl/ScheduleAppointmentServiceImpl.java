package com.tiago.service.impl;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.UUID;

import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tiago.job.AppointmentConfirmationEmailJob;
import com.tiago.job.AppointmentReminderEmailJob;
import com.tiago.payload.ScheduleAppointmentRequest;
import com.tiago.payload.ScheduleAppointmentResponse;
import com.tiago.service.ScheduleAppointmentService;
import com.tiago.util.DateUtil;

/**
 * Implements {@link ScheduleAppointmentService} interface. 
 *
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Service
public class ScheduleAppointmentServiceImpl implements ScheduleAppointmentService {

  @Autowired
  private Scheduler scheduler;
  
  private JobDataMap jobDataMap;
  
  /* (non-Javadoc)
   * @see com.tiago.service.ScheduleAppointmentService#scheduleAppointment(com.tiago.payload.ScheduleAppointmentRequest)
   */
  @Override
  public ScheduleAppointmentResponse scheduleAppointment(ScheduleAppointmentRequest scheduleAppointmentRequest) throws SchedulerException {
    buildJobDataMap(scheduleAppointmentRequest);
    
    sendAppointmentConfirmationEmail();
   
    return scheduleAppointmentReminderEmail(scheduleAppointmentRequest.getAppointmentDateTime(), scheduleAppointmentRequest.getTimeZone());
  }

  private void sendAppointmentConfirmationEmail() throws SchedulerException {
    JobDetail jobDetail = buildJobDetail();
    JobKey jobKey = JobKey.jobKey(jobDetail.getKey().getName(), jobDetail.getKey().getGroup());
    
    scheduler.addJob(jobDetail, true);
    scheduler.triggerJob(jobKey);
  }
  
  private ScheduleAppointmentResponse scheduleAppointmentReminderEmail(LocalDateTime appointmentDateTime, ZoneId zoneId) throws SchedulerException {
    ZonedDateTime zonedDateTime = ZonedDateTime.of(appointmentDateTime, zoneId); 
    JobDetail scheduledJobDetail = buildScheduledJobDetail();
    
    Trigger trigger = buildScheduledJobTrigger(scheduledJobDetail, Date.from(zonedDateTime.minusDays(1).toInstant()));
    scheduler.scheduleJob(scheduledJobDetail, trigger);
    
    return buildScheduleAppointmentResponse(scheduledJobDetail.getKey().getName(), zonedDateTime.toLocalDateTime());
  }

  private ScheduleAppointmentResponse buildScheduleAppointmentResponse(String appointmentId, LocalDateTime scheduledDateTime) {
    ScheduleAppointmentResponse scheduleAppointmentResponse = new ScheduleAppointmentResponse();
    
    scheduleAppointmentResponse.setAppointmentId(appointmentId);
    scheduleAppointmentResponse.setScheduledDateTime(scheduledDateTime);
    
    return scheduleAppointmentResponse;
  }

  private JobDetail buildJobDetail() {
    return JobBuilder.newJob(AppointmentConfirmationEmailJob.class)
        .withIdentity(UUID.randomUUID().toString(), "appointment-confirmation-email-jobs")
        .withDescription("Send Appointment Confirmation Email Job")
        .usingJobData(jobDataMap)
        .storeDurably()
        .build();
  }
  
  private JobDetail buildScheduledJobDetail() {
    return JobBuilder.newJob(AppointmentReminderEmailJob.class)
        .withIdentity(UUID.randomUUID().toString(), "appointment-reminder-email-jobs")
        .withDescription("Send Appointment Reminder Email Job")
        .usingJobData(jobDataMap)
        .storeDurably()
        .build();
  }
  
  private void buildJobDataMap(ScheduleAppointmentRequest scheduleAppointmentRequest) {
    jobDataMap = new JobDataMap();

    jobDataMap.put("name", scheduleAppointmentRequest.getName());
    jobDataMap.put("email", scheduleAppointmentRequest.getEmail());
    jobDataMap.put("scheduledDate", DateUtil.toString(scheduleAppointmentRequest.getAppointmentDateTime()));
  }
  
  private Trigger buildScheduledJobTrigger(JobDetail jobDetail, Date startAt) {
    return TriggerBuilder.newTrigger()
        .forJob(jobDetail)
        .withIdentity(jobDetail.getKey().getName(), "appointment-schedule-email-triggers")
        .withDescription("Send Appointment Schedule Email Trigger")
        .startAt(startAt)
        .build();
  }
}
