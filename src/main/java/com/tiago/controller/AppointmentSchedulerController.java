package com.tiago.controller;

import javax.validation.Valid;

import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tiago.payload.ScheduleAppointmentRequest;
import com.tiago.payload.ScheduleAppointmentResponse;
import com.tiago.service.ScheduleAppointmentService;

/**
 * Restful controller responsible for scheduling appointments.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@RestController
@RequestMapping("/api")
public class AppointmentSchedulerController {
  
  @Autowired
  ScheduleAppointmentService service;
  
  /**
   * Schedules an appointment.
   * 
   * @param scheduleAppointmentRequest
   * @return {@link ScheduleAppointmentResponse}
   * @throws SchedulerException 
   */
  @PostMapping("/scheduleAppointment")
  public ScheduleAppointmentResponse scheduleAppointment(@Valid @RequestBody ScheduleAppointmentRequest scheduleAppointmentRequest) throws SchedulerException {
    return service.scheduleAppointment(scheduleAppointmentRequest);
  }
}
